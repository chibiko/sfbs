init python:
    import random
    import re
    import time


    def randf(a, b):
        return random.uniform(a,b)

    def rand(a, b):
        if a - b >= 0 or b == 0:
            return a
        else :
            return random.randint(a,b)
    
    pobeditel = 'Nobody'
    
    field_max_x = 8
    field_max_y = 8

    def field_generation():
        global field_1
        field_1 = []
        # генерируем поле
        for i in range(0,field_max_x):
            row = {}
            for j in range(0,field_max_y):
                row[j] = rand(0,6)
            field_1.append(row)
        find_rows(0)
    
    runes = {
    0:"/runes/yellow.png",
    1:"/runes/red.png",
    2:"/runes/green.png",
    3:"/runes/blue.png",
    4:"/runes/exp.png",
    5:"/runes/money.png",
    6:"/runes/white.png",
    7:"/runes/null.png"
    }

    videlenie = 'runes/videlenie.png'

    rune_x = -1
    rune_y = -1
    rune_tmp_x = None
    rune_tmp_y = None
    slovo = 'none'
    runes_score = {0:0,1:0,2:0,3:0,4:0,5:0,6:0}
    runes_states = {0:'wind', 1:'flame', 2:'earth', 3:'water', 4:'exp', 5:'money'}

    exist_moves = 0
    cpu_moves = 0

    def who_wins():
        global pobeditel, who_move
        if cpu1.return_lives() <= 0:
            pobeditel = player1
        elif player1.return_lives() <= 0:
            pobeditel = cpu1
        else:
            pobeditel = player1
        move('loc_victory')

    
    def add_score_to_player(mana,number):
        global who_move, who_not_move
        if mana in range(0,4):
            who_move.add_mana(runes_states[mana], number)
        if mana == 4:
            who_move.add_exp(number)
        if mana == 5:
            who_move.add_money(number)
        if mana == 6:
            who_not_move.add_lives(-number)
        if mana == 7:
            exit()

    '''Тут убираем всё лишнее, падает вниз, считаем сколько очков надо нам выдать за это'''
    def zapolnenie(y,x):
        # слева направо
        for _y in range(y,-1,-1):
            if field_1[_y][x] == 7:
                if _y-1 != -1:
                    field_1[_y][x] = field_1[_y-1][x]
                    field_1[_y-1][x] = 7
                else:
                    field_1[_y][x] = rand(0,6)

    ###### ПЕРЕПИСАТЬ!!!!!!!!!!!!!!!
    ##############
    ##############
    ###############
    ##############
    def find_rows(move):
        global rune_tmp_x, rune_tmp_y, rune_x, rune_y, who_move, sobral_45, time, timer_range
        naideno = 0
        ### Переписать!!!!
        ### Проходим по всему полю по паттернам, ищем совпадения, записываем всё в большой массив
        ### После чего проходим по всему массиму и удаляем значения
        ### После чего опускаем вниз поле и так по кругу пока не будет всё найдено.

        ### подряд идущие 5,4,3
        for rows in range(5,2,-1): #### 5,4,3 (5)
            for y in range(len(field_1)-1,-1,-1): ### 7,6,5,4,3,2,1,0
                for x in range(0,len(field_1[y]) - rows + 1): ### 0,1,2,3
                    row_set = []
                    for rune in range(rows): # 0,1,2,3,4 (5)
                        row_set.append(field_1[y][x+rune]) # row_set = [6,6,6,6,6]
                    if len(set(row_set)) == 1:
                        if move:
                            if len(row_set) >= 4:
                                who_move.sobral_45 = 1
                        for rune in range(0,rows): # 0,1,2,3,4 (5)
                            if move:
                                add_score_to_player(field_1[y][x+rune],1)
                            field_1[y][x+rune] = 7
                            zapolnenie(y,x+rune)
                            naideno = 1
            for x in range(0,len(field_1[y])): ### 0,1,2,3,4,5,6,7
                for y in range(0,len(field_1) - rows + 1): ### 0,1,2,3,4
                    row_set = []
                    for rune in range(rows): # 0,1,2,3,4 (5)
                        row_set.append(field_1[y+rune][x]) # row_set = [6,6,6,6,6] (4,5,6,7,8)
                    if len(set(row_set)) == 1:
                        if move:
                            if len(row_set) >= 4:
                                who_move.sobral_45 = 1
                        for rune in range(0,rows): # 0,1,2,3,4 (5)
                            if move:
                                add_score_to_player(field_1[y+rune][x],1)
                            field_1[y+rune][x] = 7 #[3,5],[4,5],[5,5],[6,5],[7,5]
                            zapolnenie(y+rune,x)
                            naideno = 1

        if naideno:
            find_rows(move)
            time = timer_range = round_time

    def status_check(status_name):
        if who_not_move.return_status()[status_name] > 1:
            who_not_move.return_status()[status_name] -= 1
        else:
            who_not_move.return_status()[status_name] = 0

    def change_move():
        global who_move, time, who_not_move, timer_range, rune_x, rune_y
        ### Если кто-то умер, то идём пересчитывать кто победитель
        if player1.return_lives() <= 0 or cpu1.return_lives() <= 0:
            who_wins()
            return
        ### Обнуляем указатели на руны.
        rune_x = -1
        rune_y = -1
        rune_tmp_x = None
        rune_tmp_y = None
        ### Обнуляем таймер.
        time = timer_range = round_time

        ### Если кто-то собрал 4 или 5
        if who_move.get_sobral_45() == 1:
            ### Если ходил комп
            if who_move == cpu1:
                ## Он пока больше не собрал 4 или 5
                who_move.sobral_45 = 0
                ### Если ходов нет, обнуляем поле
                if cpu1.move_logic(1) == 0: #cpu_move_logic(1) == 0:
                    field_generation()
                ### После этого комп ходит
                cpu1.move_logic(0)
                find_rows(1)
                change_move()
                ### На этоми ход компа закончен
                return
                ### Если ходил не комп
            else:
                ## Обнуляем 4 или 5
                who_move.sobral_45 = 0
                ## Обнуляем таймер
                time = timer_range = round_time
                if cpu1.move_logic(1) == 0: #cpu_move_logic(1) == 0:
                    field_generation()
                #who_move = player1
                #who_not_move = cpu1
                return
        else:
            ### Обработка обычного хода
            if who_move == player1:
                if cpu1.move_logic(1) == 0: #cpu_move_logic(1) == 0:
                    field_generation()
                ## Проверка на статусы
                for status in who_not_move.return_status():
                    status_check(status)
                    if status == 'stun' and who_not_move.return_status()[status] != 0:
                        return

                who_move = cpu1
                who_not_move = player1
                
                cpu1.move_logic(0)

                find_rows(1)
                change_move()
                return
            else:
                time = timer_range = round_time
                ## Проверка на статусы
                if cpu1.move_logic(1) == 0: #cpu_move_logic(1) == 0:
                    field_generation()
                for status in who_not_move.return_status():
                    status_check(status)
                    if status == 'stun' and who_not_move.return_status()[status] != 0:
                        cpu1.move_logic(0)
                        find_rows(1)
                        change_move()
                        return

                who_move = player1
                who_not_move = cpu1
                return

    def runename(y,x):
        global rune_x, rune_y, rune_tmp_x, rune_tmp_y, slovo, who_move
        rune_tmp_x = rune_x
        rune_tmp_y = rune_y
        rune_x = x
        rune_y = y
        who_move.set_sobral_45 = 0
        if slovo != 'none':
            if (rune_tmp_y + 1 == rune_y and rune_tmp_x == rune_x) or (rune_tmp_y - 1 == rune_y and rune_tmp_x == rune_x) or (rune_tmp_x + 1 == rune_x and rune_tmp_y == rune_y) or (rune_tmp_x - 1 == rune_x and rune_tmp_y == rune_y):
                tmp_rune = field_1[y][x]
                field_1[y][x] = field_1[rune_tmp_y][rune_tmp_x]
                field_1[rune_tmp_y][rune_tmp_x] = tmp_rune
                slovo = 'none'
                if who_move == player1:
                    ###################################
                    if rows_in_field() == 0:
                        if difficult != -1:
                            player1.add_lives(-5)
                        tmp_rune = field_1[y][x]
                        field_1[y][x] = field_1[rune_tmp_y][rune_tmp_x]
                        field_1[rune_tmp_y][rune_tmp_x] = tmp_rune        
                    ###################################
                    find_rows(1)
                    rune_x = -1
                    rune_y = -1
                    rune_tmp_x = None
                    rune_tmp_y = None
                    if who_move.get_sobral_45() == 0:
                        if cpu1.move_logic(1) == 0: #cpu_move_logic(1) == 0:
                            field_generation()
                        change_move()
                    else:
                        who_move.sobral_45 = 0
        else:
            slovo = 'hod'
        rune_tmp_y = None
        rune_tmp_x = None


    def rows_in_field():
        for y in range(len(field_1)-1,-1,-1): ### 7,6,5,4,3,2,1,0
            for x in range(0,len(field_1[y]) - 2): ### 0,1,2,3
                row_set = []
                for rune in range(3): # 0,1,2,3,4 (5)
                    row_set.append(field_1[y][x+rune]) # row_set = [6,6,6,6,6]
                if len(set(row_set)) == 1:
                    return 1
        for x in range(0,len(field_1[y])): ### 0,1,2,3,4,5,6,7
            for y in range(0,len(field_1) - 2): ### 0,1,2,3,4
                row_set = []
                for rune in range(3): # 0,1,2,3,4 (5)
                    row_set.append(field_1[y+rune][x]) # row_set = [6,6,6,6,6] (4,5,6,7,8)
                if len(set(row_set)) == 1:
                    return 1
        return 0

