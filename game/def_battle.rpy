init python:
    def timer_off(lives):
        if difficult == 1:
            who_move.add_lives(-lives)

transform alpha_dissolve:
    alpha 0.0
    linear 0.5 alpha 1.0
    on hide:
        linear 0.5 alpha 0
    # This is to fade the bar in and out, and is only required once in your script

screen countdown:
    timer 0.01 repeat True action If(time > 0, true=[SetVariable('time', time - 0.01)], false=[(Function(timer_off,3),Function(change_move))])
    bar value time range timer_range xalign 0.5 yalign 0.95 xmaximum 300 at alpha_dissolve # This is the timer bar.


screen phase:
    vbox:
        xalign 0.5
        ypos 0.80
        text _("Ход: [who_move.name]")
        if player1.return_lives() > cpu1.return_lives():
            $ abdajbdjabd = 'побеждаете'
        elif player1.return_lives() == cpu1.return_lives():
            $ abdajbdjabd = 'идёте вровень'
        else:
            $ abdajbdjabd = 'проигрываете'
        text _("Вы [abdajbdjabd].")