screen stats_screen:
    # tag interface
    # add 'pic/overlay.png'
        ## Pick locations
    key "game_menu" action NullAction()
    fixed:
        for lab, pick, act, req in loc_pick:
            if req:
                imagebutton auto str(pick+"_%s.png") xpos 0 ypos 0 focus_mask True action act
    fixed xpos 0.01 ypos 0.01:
        vbox xmaximum config.screen_width/2:
            
            null height 10


    vbox xalign 0.99 yalign 0.0:
        

        # text '{u}Действия:{/u}' style style.param xalign 0.99
        if loc_btn and len(loc_btn) > 0:
            for lab, act, req in loc_btn :
                if req:
                    if lab[:3] != '{i}':
                        textbutton lab:
                            xalign 0.99
                            action act
                            style "navigation_button" text_style "navigation_button_text"
                    else:
                        textbutton lab:
                            xalign 0.99
                            action act
                            style "navigation_button" text_style "action_button_text"