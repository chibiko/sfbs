init python:
    class Skills():
        def __init__(self,skillname,cost,fromlevel,about,opisanie,damage=0,moves=0,status=''):
            self.skillname = skillname
            self.cost = cost
            self.fromlevel = fromlevel
            self.about = about
            self.opisanie = opisanie
            self.damage = damage
            self.moves = moves
            self.status = status

        def __repr__(self):
            return '<{} name: "{}">'.format(self.__class__.__name__,
                                            self.__name__('utf-8'))


        def return_skill_name(self):
            return self.skillname

        def return_skill_cost(self):
            return self.cost

        def return_skill_opisanie(self):
            return self.opisanie

        def about(self):
            return self.about

        def return_level(self):
            return self.fromlevel

        def del_mana_on_use_skill(self):
            for rune,manacost in self.cost:
                who_move.add_mana(runes_states[rune],-manacost)

        def skill_check_cost(self):
            for rune,manacost in self.cost:
                if who_move.return_mana(runes_states[rune]) < manacost:
                    return False
            return True


        #######################################
        #######################################
        ##### SKILLS ##########################
        #######################################

        def user_skill_use(self):
            if self.skill_check_cost():
                self.del_mana_on_use_skill()

                if 'damage' in self.opisanie:
                    who_not_move.add_lives(-self.damage)

                if 'heal' in self.opisanie:
                    who_move.add_lives(self.damage)
                    if who_move.return_lives() > who_move.return_maxlives():
                        who_move.lives = who_move.return_maxlives()

                if 'status' in self.opisanie:
                    who_not_move.add_status(self.status,self.moves)


        def skill_use_try(self, who_check):
            if who_check == player1:
                for rune,manacost in self.cost:
                    if player1.return_mana(runes_states[rune]) < manacost:
                        return False
                return True
            else:
                for rune,manacost in self.cost:
                    if cpu1.return_mana(runes_states[rune]) < manacost:
                        return False
                return True



    class Methods(Skills):
        pass
