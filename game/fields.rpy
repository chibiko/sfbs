transform videlenie_1:
    subpixel True
    im.Scale(videlenie,60,60)
    rotate 0
    linear 3.0 rotate 360
    repeat

screen battle_field:
    key "mouseup_3" action Function(cpu1.runes_zero)
    key "K_ESCAPE" action Show('pause')
    ##### Player Phase
    vbox:
        #xpos 0.25
        ypos 0.2
        xalign 0.5
        grid len(field_1[0]) len(field_1):
            for y in range(0,len(field_1)):
                for x in range(0,len(field_1[y])):
                    imagebutton:
                        idle im.Scale(runes[field_1[y][x]], 50, 50)
                        hover im.Scale(runes[field_1[y][x]], 50, 50)
                        if who_move == player1:
                            action [Function(runename,y,x)]
                        else:
                            action NullAction()
    vbox:
        ypos 0.1798
        xpos 0.2912
        if rune_x >= 0 and rune_y >= 0:
            add videlenie_1 xpos ((rune_x * 50) - 3) ypos ((rune_y * 50) - 2)


    vbox:
        xpos 0.025
        ypos 0.025
        $ player_statuses = ''
        for status in player1.return_status():
            if status == 'stun':
                $ player_statuses += 'O' * player1.return_status()[status]

        $ rune_name = 'Имя игрока:\n{0} {1}\nКласс: {2}\nУровень: {3}\nФея: Огненная фея\nСтатусы: {4}\nМана:\n'.format(
            player1.name,player1.fname,player1.char_class, player1.level, player_statuses)
        text _([rune_name])
        for i in range(0,6):
            hbox:
                image im.Scale(runes[i],20,20)
                if i in range(0,4):
                    text _(": " + str(player1.return_mana(runes_states[i])))
                elif i == 4:
                    text _(": " + str(player1.return_exp()))
                elif i == 5:
                    text _(": " + str(player1.return_money()))
        hbox:
            image im.Scale('/runes/heart.png',20,20)
            text _(": " + str(player1.return_lives()))

    vbox:
        xpos 0.025
        ypos 0.52
        text _('Навыки:')
        for skill in player1.return_skillbook():
            if skill.return_level() <= player1.return_level():
                if skill.skill_use_try(player1):
                    textbutton skill.return_skill_name() action [Function(skill.user_skill_use),Function(change_move)]
                else:
                    $ skillname = skill.return_skill_name()
                    text _("[skillname]")
                hbox:
                    for titles,cost in skill.return_skill_cost():
                        add im.Scale('{0}'.format(runes[titles]),20,20)
                        text (' : [cost]  ')


    #### CPU 
    vbox:
        xpos 0.025
        ypos 0.61
        for skill in player1.return_fairybook():
            if skill.return_level() <= player1.return_level():
                if skill.skill_use_try(player1):
                    textbutton skill.return_skill_name() action [Function(skill.user_skill_use),Function(change_move)]
                else:
                    $ skillname = skill.return_skill_name()
                    text _("[skillname]")
                hbox:
                    for titles,cost in skill.return_skill_cost():
                        add im.Scale('{0}'.format(runes[titles]),20,20)
                        text (' : [cost]  ')

    ##### Monster Phase
    vbox:
        xpos 0.8
        ypos 0.025
        $ monster_statuses = ''
        for status in cpu1.return_status():
            if status == 'stun':
                $ monster_statuses += '{color=666}O{/color}' * cpu1.return_status()[status]
        $ monster_name = 'Имя монстра:\n{0}\n{1}\nКласс: {2}\nУровень: {3}\nСтатус: {4}\nМана:\n'.format(
            cpu1.name, cpu1.fname, cpu1.char_class, cpu1.level, monster_statuses)
        text _([monster_name])
        for i in range(0,6):
            hbox:
                xpos 0.8
                image im.Scale(runes[i],20,20)
                if i in range(0,4):
                    text _(": " + str(cpu1.return_mana(runes_states[i])))
                elif i == 4:
                    text _(": " + str(cpu1.return_exp()))
                elif i == 5:
                    text _(": " + str(cpu1.return_money()))
        hbox:
            xpos 0.8
            image im.Scale('/runes/heart.png',20,20)
            text _(": " + str(cpu1.return_lives()))

    vbox:
        xpos 0.75
        ypos 0.52
        text _('Навыки:')
        for skill in cpu1.return_skillbook():
            if skill.return_level() <= cpu1.return_level():
                if skill.skill_use_try(cpu1):
                    textbutton skill.return_skill_name() action NullAction()
                else:
                    $ skillname = skill.return_skill_name()
                    text _("[skillname]")
                hbox:
                    for titles,cost in skill.return_skill_cost():
                        add im.Scale('{0}'.format(runes[titles]),20,20)
                        text (' : [cost]  ')

    vbox:
        xpos 0.8
        ypos 0.9
        textbutton 'Настройки' action ShowMenu("preferences")


    vbox:
        ypos 0.9
        if config.developer:
            textbutton 'change_move' action [Function(change_move)]
            textbutton 'generate_field' action [Function(field_generation)]


screen pause:
    key "K_ESCAPE" action [Hide('pause')]
    key "mouseup_1" action NullAction()
    #add im.Scale('images/world_map/world_map.jpg',1024,768)
    add '#000'
    text _('Рекламная пауза') xalign 0.5 yalign 0.5