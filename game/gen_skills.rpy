init python:

    skillbook = []
    fairy_magicbook = []
    
    ## Class skills

    warrior_skill = Skills('Убийственный удар', [(1,4),(3,8)], 1, 'Скилл такой-то',['damage'],6)
    skillbook.append(warrior_skill)
    warrior_skillbook.append(warrior_skill)
    
    cleric_skill = Skills('Спасительные жизни',[(0,4),(3,4)],1,'Лечение',['heal'],3)
    skillbook.append(cleric_skill)
    cleric_skillbook.append(cleric_skill)

    ## Fairie skills
    fireball = Skills('Огненный шар',[(1,4),(0,2)],1,'Кидание огненных шаров',['damage'],4)
    fairy_magicbook.append(fireball)

    headshot = Skills('Удар по голове',[(0,4),(2,12)],1,'Удар по голове и оглушение',['damage','status'],damage=2,moves=3,status='stun')
    warrior_skillbook.append(headshot)
    
    vampire_bite = Skills('Укус вампира',[(3,6),(2,4)],3,'Кидание огненных шаров',['damage','heal'],2)
    #fairy_magicbook.append(vampire_bite)