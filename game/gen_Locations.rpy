# -*- coding: utf-8 -*-
init 10 python:
    locations = []
    alllocks = {}
    global loc_btn, loc_txt, loc_pick
    loc_btn = []
    loc_pick = {}
    loc_txt = []

    class Location:
        def __init__(self, id, name, base_prob, position, mapname, button):
            self.id = id
            self.name = name
            self.base_prob = base_prob
            self.position = position
            self.mapname = mapname
            self.button = button
            self.items = []
            self.__statuses = []

        def __repr__(self):
            return '<{} name: "{}">'.format(self.__class__.__name__,
                                            self.__name__('utf-8'))
    def getLoc(id):
        for x in locations:
            if x.id == id:
                return x
        return False

    def getBut(id):
        for x in locations:
            if x.id == id:
                return x.button
        return False

    def changeBut(id):
        for x in locations:
            if x.id == id:
                if x.button == 1:
                    x.button = 0
                else:
                    x.button = 1
                #return False
    # Создание массива всех локаций
    _locs = renpy.get_all_labels()
    def genLocs():

        for x in _locs:
            if x[:4] == 'loc_':
                if   x == 'loc_home':     loc = Location(id = x, name = 'home', base_prob = -1, position = ['home','safe'], mapname = {"mapname":"myhome","x":0,"y":0}, button = 0 )
                elif x == 'loc_battle':   loc = Location(id = x, name = 'battle', base_prob = -1, position = ['home','safe'], mapname = {"mapname":"battle","x":0,"y":0}, button = 0 )
                elif x == 'loc_victory':  loc = Location(id = x, name = 'victory', base_prob = -1, position = ['home','safe'], mapname = {"mapname":"victory","x":0,"y":0}, button = 0 )
                elif x == 'loc_myhelp':   loc = Location(id = x, name = 'myhelp', base_prob = -1, position = ['home','safe'], mapname = {"mapname":"myhelp","x":0,"y":0}, button = 0 )
                elif x == 'loc_admins':   loc = Location(id = x, name = 'admins', base_prob = -1, position = ['home','safe'], mapname = {"mapname":"admins","x":0,"y":0}, button = 0 )
                elif x == 'loc_create':   loc = Location(id = x, name = 'create', base_prob = -1, position = ['home','safe'], mapname = {"mapname":"create","x":0,"y":0}, button = 0 )
                else: loc = Location(id = x, name = 'UNKNOWN', base_prob = -1, position = ['other'])
                locations.append(loc)

    genLocs() # генерирую локации
    
######################################################
# Объявление всех картинок локаций
init:
    image home = im.Scale('images/world_map/world_map.jpg',1024,768)
    image battle = 'images/world_map/bfield.png' # 
    image victory = '#000' # 
    image myhelp = '#000' #  
    image admins = '#000' #  
    image create = '#000' #  
    
##############################################################
# Home
##############################################################
### Прихожая
label loc_home:
    #if ptime == 0:
    #    $ ptime += 1
    #    $ move ('intro')
    show home
    python:
        loc_btn = [
                    ('{i}Настройки{/i}', Show('preferences'), True),
                    ('{i}Правила игры{/i}', Function(move,'loc_myhelp'), True),
                    ('{i}Битва{/i}', Function(move,'loc_battle'), True),
                    ('{i}Тесты{/i}', Function(move,'loc_admins'), config.developer == True),
        ]
        loc_pick = []
        loc_txt = ['Главный экран. ']
    screen home:
        text _("")
    call screen home

### Прихожая
label loc_create:
    #if ptime == 0:
    #    $ ptime += 1
    #    $ move ('intro')
    show create
    python:
        loc_btn = [
                    ('{i}Настройки{/i}', Show('preferences'), True),
        ]
        loc_pick = []
        loc_txt = ['Главный экран. ']
    screen create:
        vbox:
            text _("Создание персонажа.")
        vbox:
            text _("Имя персонажа.")




    call screen create


label loc_battle:
    #if ptime == 0:
    #    $ ptime += 1
    #    $ move ('intro')
    show battle
    show screen phase
    show screen battle_field
    hide screen stats_screen
    $ timer_range = time = round_time
    show screen countdown
    python:
        loc_btn = [
                    ('{i}Настройки{/i}', Show('preferences'), True),
        ]
        loc_pick = []
        loc_txt = ['Экран битвы. ']
    screen battle:
        pass
    call screen battle

label loc_victory:
    if pobeditel == player1:
        $ cpu1.add_maxlives(3)
    else:
        $ cpu1.add_maxlives(-3)
    $ player1.lives = player1.return_maxlives()
    $ cpu1.lives = cpu1.return_maxlives()
    $ who_moves = player1
    $ who_not_moves = cpu1
    show victory
    hide screen countdown
    python:
        loc_btn = [
                    ('{i}Настройки{/i}', Show('preferences'), True),
                    ('{i}Битва{/i}', Function(move,'loc_battle'), True),
                    ('{i}Правила игры{/i}', Function(move,'loc_myhelp'), pobeditel == cpu1),
                    ('{i}Выход{/i}', Quit(confirm=False), True),
        ]
        loc_pick = []
        loc_txt = ['Экран победы. ']
    screen victory:
        vbox:
            xalign 0.5
            ypos 0.3
            if pobeditel == player1:
                text _("Вы победили!")
            else:
                text _("Вы проиграли!")
            text _("Попробовать ещё раз? Кнопка наверху справа :)")
    call screen victory

label loc_myhelp:
    show myhelp
    python:
        loc_btn = [
                    ('{i}Настройки{/i}', Show('preferences'), True),
                    ('{i}Битва{/i}', Function(move,'loc_battle'), True),
                    ('{i}Обратно{/i}', Function(move,'loc_home'), True),
                    ('{i}Выход{/i}', Quit(confirm=False), True),
        ]
        loc_pick = []
        loc_txt = ['Простейшие правила игры.']
    screen myhelp:
        vbox:
            xpos 0.05
            xmaximum 0.9
            ypos 0.05
            $ alltext = "Игра из серии игр \"3 в ряд\", где надо собрать одинаковые тайтлы в ряд, передвигая и меняя местами соседние тайтлы по горизонтали или вертикали. \n\
Сами тайтлы делятся на 4 вида: \n\
1) Мана. Бывает: \n\
{image=[runes[1]]} огненной \n\
{image=[runes[2]]} земли \n\
{image=[runes[0]]} воздуха \n\
{image=[runes[3]]} и воды. \n\
За ману можно будет покупать заклинания. В данный момент не используется. \n\
2) Опыт: \n\
{image=[runes[4]]} За опыт впоследствии можно будет покупать заклинания. В данный момент не используется. \n\
3) Деньги.  \n\
{image=[runes[5]]} За деньги можно будет покупать экипировку, зелья, и так далее. В данный момент не используется. \n\
4) Черепа битвы. \n\
{image=[runes[6]]} Если собрать их в ряд (3,4,5 шт.), то у противника уменьшится запас жизней, соответствующий количеству собранных черепов. \n\
Цель поединка - убить противника, доведя его количество жизней до 0. \n\
Собрав 4 или 5 тайтлов в ряд, ход остаётся у вас. Компьютерный игрок часто этим пользуется!"
            text _("[alltext]")
    call screen myhelp



label loc_admins:
    show admins
    e 'Это страничка тестирования серверной и клиентской части...'
    e 'Мне кажется, вам тут нечего делать...'
    python:
        loc_btn = [
                    ('{i}Настройки{/i}', Show('preferences'), True),
                    ('{i}Обратно{/i}', Function(move,'loc_home'), True),
                    ('{i}Выход{/i}', Quit(confirm=False), True),
        ]
        loc_pick = []
        loc_txt = ['Экран битвы.']
    screen admins:
        vbox:
            textbutton '{i}Включить серверную часть{/i}' action Function(renpy.invoke_in_thread,create_cocket)
            textbutton '{i}Выключить серверную часть{/i}' action SetVariable('socket_exit',1)
            textbutton '{i}Соедениться с сервером{/i}' action Function(renpy.invoke_in_thread,client_connect)
        vbox:
            xalign 0.5
            ypos 0.2
            text _("Состояние:")
            text _("[socket_print]")

    call screen admins