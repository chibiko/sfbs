init python:
#базовая функция перемещения. Использовать всегда и всюду
    from random import shuffle
    def move(where,*args):
        global curloc, prevloc, same_loc
        if renpy.has_label(where) == True: #Проверка на то, что локация существует. Если нет, прыгаем домой.
            renpy.scene(layer='master') # Сброс картинок
            renpy.scene(layer='screens') # Сброс скринов
            #renpy.show('daytime') # Базовый фон
            #player.incEnergy(randf(2,5)) #расход энергии
            #resetStats(allChars) #Сброс статов
            #player1.checkDur() # Удаление использованных предметов

            # Переходы с технических локаций и на технические локации н
                    #cpu_shodil = '5.1.1'е занимают времени
            # if (curloc.startswith('loc_') and 'tech' not in getLoc(curloc).position)\
                # and (where.startswith('loc') and 'tech' not in getLoc(where).position)\
                # and curloc != where:
                
            #changetime(rand(1, 3)) #изменение времени
            if where[:4] == 'loc_' and 'tech' not in getLoc(where).position: #Если локация - локация и если она не техническая
                #checkDeath() # проверка на смерть
                if where != curloc and 'self' not in getLoc(where).position:
                    prevloc = curloc
                    curloc = where
                    same_loc = 0
                else:
                    same_loc = 1
                if 'self' not in getLoc(where).position:
                    renpy.show_screen('stats_screen') #При перемещении всегда появляется интерфейс
                tempLoc = getLoc(where)
                #checkClothes(where) # проверка одетости
                #checkUnconscious(tempLoc) # потеря сознания
                #checkSperm(tempLoc) # снятие репутации за сперму.
                #checkOrgasm(tempLoc) # проверка на перевозбуждение
                #checkPhone() # Проверка на телефонные звонки
                #checkMisc() # Прочие мелкие проверки
            
            #if rand(1,100) < 10 + noEventTime and len(getLoc(curloc).getPeople()) > 0 and lastEventTime + 15 < mtime: #and  same_loc == 0: # Если на локации кто то есть и локация поменялась, дёргаем эвент по рандому
            #    tryEvent(where) # попытка дёрнуть рандомный эвент с локации. Ожидание не даёт эвентов.
            renpy.retain_after_load() # чтобы сохранялся интерфейс, иначе ошибка
            
            #if  where[:4] == 'loc_': trySpecialEvent(where) # спец эвент
            #if len(args) > 0:
            #    changetime(args[0])

            renpy.jump(where) #Переход на локу
        else:
            renpy.jump('loc_home')
