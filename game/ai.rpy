init -1 python:
    class AI(Monster):
        global exist_moves


        def runes_zero(self):
            global rune_x, rune_y, rune_tmp_x, rune_tmp_y
            rune_x = -1
            rune_y = -1
            rune_tmp_x = None
            rune_tmp_y = None


        def move_logic(self, moves_only):    
            ## Ищем все возможные 5
            # Все горизонтали сверху вниз:
            exist_moves = {}
            i = 0
            for y in range(0,len(field_1)-1):
                for x in range(0,len(field_1[y]) - 4):
                    if field_1[y][x] == field_1[y][x+1] == field_1[y+1][x+2] == field_1[y][x+3] == field_1[y][x+4]:
                        exist_moves[i] = { 5 : { field_1[y][x] : [ ( y+1 , x+2 ) , ( y , x+2 ) ] } }
                        i += 1
                        #runename(y+1,x+2)
                        #runename(y,x+2)
                        #cpu_shodil = '5.1.1'
                        #cpu_moves += 1
                        #return
            # Все горизонтали снизу вверх:
            for y in range(len(field_1)-1,0,-1):
                for x in range(0,len(field_1[y]) - 4):
                    if field_1[y][x] == field_1[y][x+1] == field_1[y-1][x+2] == field_1[y][x+3] == field_1[y][x+4]:
                        exist_moves[i] = {5:{field_1[y][x]:[(y-1,x+2),(y,x+2)]}}
                        i += 1
                        #runename(y-1,x+2)
                        #runename(y,x+2)
                        #cpu_shodil = '5.1.1'
                        #cpu_moves += 1
                        #return
            # Все вертикальные слева направо:
            for x in range(0,len(field_1[0])-1):
                for y in range(0,len(field_1) - 4):
                    if field_1[y][x] == field_1[y+1][x] == field_1[y+2][x+1] == field_1[y+3][x] == field_1[y+4][x]:
                        exist_moves[i] = {5:{field_1[y][x]:[(y+2,x+1),(y+2,x)]}}
                        i += 1
                        #runename(y+2,x+1)
                        #runename(y+2,x)
                        #cpu_shodil = '5.2.1'
                        #cpu_moves += 1
                        #return
            # вертикальные справа налево:
            for x in range(len(field_1[0])-1,0,-1):
                for y in range(0,len(field_1) - 4):
                    if field_1[y][x] == field_1[y+1][x] == field_1[y+2][x-1] == field_1[y+3][x] == field_1[y+4][x]:
                        exist_moves[i] = {5:{field_1[y][x]:[(y+2,x-1),(y+2,x)]}}
                        i += 1
                        #runename(y+2,x-1)
                        #runename(y+2,x)
                        #cpu_shodil = '5.2.1'
                        #cpu_moves += 1
                        #return
            ## Ищем все возможные 4
            ## Слева направо, сверху вниз
            for y in range(0,len(field_1)-1):
                for x in range(0,len(field_1[y]) - 3):
                    if field_1[y][x] == field_1[y][x+1] == field_1[y+1][x+2] == field_1[y][x+3]:
                        exist_moves[i] = {4:{field_1[y][x]:[(y+1,x+2),(y,x+2)]}}
                        i += 1
                        #runename(y+1,x+2)
                        #runename(y,x+2)
                        #cpu_shodil = '4.1.1'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x] == field_1[y+1][x+1] == field_1[y][x+2] == field_1[y][x+3]:
                        exist_moves[i] = {4:{field_1[y][x]:[(y+1,x+1),(y,x+1)]}}
                        i += 1
                        #runename(y+1,x+1)
                        #runename(y,x+1)
                        #cpu_shodil = '4.1.2'
                        #cpu_moves += 1
                        #return
            for y in range(len(field_1)-1,0,-1):
                for x in range(0,len(field_1[y]) - 3):
                    if field_1[y][x] == field_1[y][x+1] == field_1[y-1][x+2] == field_1[y][x+3]:
                        exist_moves[i] = {4:{field_1[y][x]:[(y-1,x+2),(y,x+2)]}}
                        i += 1
                        #runename(y-1,x+2)
                        #runename(y,x+2)
                        #cpu_shodil = '4.2.1'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x] == field_1[y-1][x+1] == field_1[y][x+2] == field_1[y][x+3]:
                        exist_moves[i] = {4:{field_1[y][x]:[(y-1,x+1),(y,x+1)]}}
                        i += 1
                        #runename(y-1,x+1)
                        #runename(y,x+1)
                        #cpu_shodil = '4.2.2'
                        #cpu_moves += 1
                        #return
            ## вертикалки
            for x in range(0,len(field_1[0])-1):
                for y in range(0,len(field_1) - 3):
                    if field_1[y][x] == field_1[y+1][x] == field_1[y+2][x+1] == field_1[y+3][x]:
                        exist_moves[i] = {4:{field_1[y][x]:[(y+2,x+1),(y+2,x)]}}
                        i += 1
                        #runename(y+2,x+1)
                        #runename(y+2,x)
                        #cpu_shodil = '4.3.1'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x] == field_1[y+1][x+1] == field_1[y+2][x] == field_1[y+3][x]:
                        exist_moves[i] = {4:{field_1[y][x]:[(y+1,x+1),(y+1,x)]}}
                        i += 1
                        #runename(y+1,x+1)
                        #runename(y+1,x)
                        #cpu_shodil = '4.3.2'
                        #cpu_moves += 1
                        #return
            ## По горизонтали
            for x in range(len(field_1[0])-1,0,-1):
                for y in range(0,len(field_1) - 3):
                    if field_1[y][x] == field_1[y+1][x] == field_1[y+2][x-1] == field_1[y+3][x]:
                        exist_moves[i] = {4:{field_1[y][x]:[(y+2,x-1),(y+2,x)]}}
                        i += 1
                        #runename(y+2,x-1)
                        #runename(y+2,x)
                        #cpu_shodil = '4.4.1'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x] == field_1[y+1][x-1] == field_1[y+2][x] == field_1[y+3][x]:
                        exist_moves[i] = {4:{field_1[y][x]:[(y+1,x-1),(y+1,x)]}}
                        i += 1
                        #runename(y+1,x-1)
                        #runename(y+1,x)
                        #cpu_shodil = '4.4.1'
                        #cpu_moves += 1
                        #return

            ## Ищем все возможные 3
            # Все горизонтали сверху вниз:
            ## Слева направо, сверху вниз xx-x и x-xx
            for y in range(0,len(field_1)-1):
                for x in range(0,len(field_1[y]) - 3):
                    if field_1[y][x] == field_1[y][x+1] == field_1[y][x+3]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y,x+3),(y,x+2)]}}
                        i += 1
                        #runename(y,x+3)
                        #runename(y,x+2)
                        #cpu_shodil = '3.0.1'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x] == field_1[y][x+2] == field_1[y][x+3]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y,x),(y,x+1)]}}
                        i += 1
                        #runename(y,x)
                        #runename(y,x+1)
                        #cpu_shodil = '3.0.2'
                        #cpu_moves += 1
                        #return
            ## Всякие другие фигуры
            for y in range(0,len(field_1)-1):
                for x in range(0,len(field_1[y]) - 2):
                    if field_1[y][x] == field_1[y+1][x+1] == field_1[y][x+2]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y+1,x+1),(y,x+1)]}}
                        i += 1
                        #runename(y+1,x+1)
                        #runename(y,x+1)
                        #cpu_shodil = '3.1.1'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x] == field_1[y][x+1] == field_1[y+1][x+2]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y+1,x+2),(y,x+2)]}}
                        i += 1
                        #runename(y+1,x+2)
                        #runename(y,x+2)
                        #cpu_shodil = '3.1.2'
                        #cpu_moves += 1
                        #return
                    elif field_1[y+1][x] == field_1[y][x+1] == field_1[y][x+2]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y+1,x),(y,x)]}}
                        i += 1
                        #runename(y+1,x)
                        #runename(y,x)
                        #cpu_shodil = '3.1.3'
                        #cpu_moves += 1
                        #return
            # Все горизонтали снизу вверх:
            for y in range(len(field_1)-1,0,-1):
                for x in range(0,len(field_1[y]) - 2):
                    if field_1[y][x] == field_1[y-1][x+1] == field_1[y][x+2]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y-1,x+1),(y,x+1)]}}
                        i += 1
                        #runename(y-1,x+1)
                        #runename(y,x+1)
                        #cpu_shodil = '3.2.1'
                        #cpu_moves += 1
                        #return
                    elif field_1[y-1][x] == field_1[y][x+1] == field_1[y][x+2]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y-1,x),(y,x)]}}
                        i += 1
                        #runename(y-1,x)
                        #runename(y,x)
                        #cpu_shodil = '3.2.2'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x] == field_1[y][x+1] == field_1[y-1][x+2]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y-1,x+2),(y,x+2)]}}
                        i += 1
                        #runename(y-1,x+2)
                        #runename(y,x+2)
                        #cpu_shodil = '3.2.3'
                        #cpu_moves += 1
                        #return
            # Все вертикальные слева направо:
            for x in range(0,len(field_1[0])-1):
                for y in range(0,len(field_1) - 3):
                    if field_1[y][x] == field_1[y+1][x] == field_1[y+3][x]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y+3,x),(y+2,x)]}}
                        i += 1
                        #runename(y+3,x)
                        #runename(y+2,x)
                        #cpu_shodil = '3.0.3'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x] == field_1[y+2][x] == field_1[y+3][x]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y,x),(y+1,x)]}}
                        i += 1
                        #runename(y,x)
                        #runename(y+1,x)
                        #cpu_shodil = '3.0.4'
                        #cpu_moves += 1
                        #return
            for x in range(0,len(field_1[0])-1):
                for y in range(0,len(field_1) - 2):
                    if field_1[y][x] == field_1[y+1][x+1] == field_1[y+2][x]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y+1,x+1),(y+1,x)]}}
                        i += 1
                        #runename(y+1,x+1)
                        #runename(y+1,x)
                        #cpu_shodil = '3.3.1'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x] == field_1[y+1][x] == field_1[y+2][x+1]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y+2,x+1),(y+2,x)]}}
                        i += 1
                        #runename(y+2,x+1)
                        #runename(y+2,x)
                        #cpu_shodil = '3.3.2'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x+1] == field_1[y+1][x] == field_1[y+2][x]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y,x+1),(y,x)]}}
                        i += 1
                        #runename(y,x+1)
                        #runename(y,x)
                        #cpu_shodil = '3.3.3'
                        #cpu_moves += 1
                        #return
            # вертикальные справа налево:
            for x in range(len(field_1[0])-1,0,-1):
                for y in range(0,len(field_1) - 2):
                    if field_1[y][x] == field_1[y+1][x-1] == field_1[y+2][x]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y+1,x-1),(y+1,x)]}}
                        i += 1
                        #runename(y+1,x-1)
                        #runename(y+1,x)
                        #cpu_shodil = '3.4.1'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x] == field_1[y+1][x] == field_1[y+2][x-1]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y+2,x-1),(y+2,x)]}}
                        i += 1
                        #runename(y+2,x-1)
                        #runename(y+2,x)
                        #cpu_shodil = '3.4.2'
                        #cpu_moves += 1
                        #return
                    elif field_1[y][x-1] == field_1[y+1][x] == field_1[y+2][x]:
                        exist_moves[i] = {3:{field_1[y][x]:[(y,x-1),(y,x)]}}
                        i += 1
                        #runename(y,x-1)
                        #runename(y,x)
                        #cpu_shodil = '3.4.3'
                        #cpu_moves += 1
                        #return
            if moves_only:
                return len(exist_moves)
            else:
                ## Обозначаем лист с возможными ходами на 3,4 или 5
                runes_minilist = {}
                ## Берем максимальный возможный ход
                runes_max = max([exist_moves[iter].keys() for iter in exist_moves])[0]
                ## Для всех возможных ходов
                for iterables in exist_moves:
                    ## Если оно равно максимальной руне
                    if exist_moves[iterables].keys()[0] == runes_max:
                        ## Добавляем максимальные руны в словарь с ходами
                        runes_minilist[iterables] = exist_moves[iterables][runes_max].keys()[0]
                ## Если 
                if runes_max in [4,5]:
                    for iterables in runes_minilist:
                        if runes_minilist[iterables] == 6:
                            runename(exist_moves[iterables][runes_max].values()[0][0][0],exist_moves[iterables][runes_max].values()[0][0][1])
                            runename(exist_moves[iterables][runes_max].values()[0][1][0],exist_moves[iterables][runes_max].values()[0][1][1])
                            self.runes_zero()
                            return
                    for iterables in runes_minilist:
                        runename(exist_moves[iterables][runes_max].values()[0][0][0],exist_moves[iterables][runes_max].values()[0][0][1])
                        runename(exist_moves[iterables][runes_max].values()[0][1][0],exist_moves[iterables][runes_max].values()[0][1][1])
                        self.runes_zero()
                        return

                ## теперь проверяем тройки - есть ли черепки.
                for iterables in runes_minilist:
                    if runes_minilist[iterables] == 6:
                        runename(exist_moves[iterables][runes_max].values()[0][0][0],exist_moves[iterables][runes_max].values()[0][0][1])
                        runename(exist_moves[iterables][runes_max].values()[0][1][0],exist_moves[iterables][runes_max].values()[0][1][1])
                        self.runes_zero()
                        return

                ###Время скиллов!
                ### 66% что долбанёт скиллом!!!
                if 6 not in runes_minilist:
                    if rand(0,2):
                        for skill in cpu1.return_skillbook():
                            if skill.skill_use_try(cpu1):
                                skill.user_skill_use()
                                return

                ## Определить сбор ресурсов по приоритетам надо бы тут!!!!!
                ## Всё правильноЮ сначала черепки
                for skill in who_move.return_skillbook():
                    ### пишем ТЗ.
                    # Вычисляем какие камни нужны.
                    # Вычисляем какие камни не хватает
                    # Проверка на камни в поле
                    # Сбор камней
                    for need_mana, need_value in skill.return_skill_cost():
                        if need_value > cpu1.return_my_mana(need_mana):
                            pass
                    for iterables in runes_minilist:
                        runename(exist_moves[iterables][runes_max].values()[0][0][0],exist_moves[iterables][runes_max].values()[0][0][1])
                        runename(exist_moves[iterables][runes_max].values()[0][1][0],exist_moves[iterables][runes_max].values()[0][1][1])
                        self.runes_zero()
                        return




