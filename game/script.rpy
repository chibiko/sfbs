﻿init python:
    #config.keymap['dismiss'].append('t')
    config.keymap['game_menu'].remove('mouseup_3')
    config.keymap['game_menu'].remove('K_ESCAPE')

    pause_button = 0

    round_time = 5
    difficult = 0
    easy = 0
    monsters = []
    tips_on = 1
    tips_battle = 1
    player1 = Feya(mana = {0:0, 1:0, 2:0, 3:0}, money = 0, maxlives = 20, lives = 20, exp = 0, 
        char_class = 'cleric', iniciative = 3, level = 1, icon = None, status = {})
    player1.initname('Jenifer', 'Delaylah O\'hara', sobral_45 = 0, feyalives = 20, feyaexp = 0)

    orc = AI(mana = {0:0, 1:0, 2:0, 3:0}, money = 0, maxlives = 26, lives = 24, exp = 0, 
        char_class = 'warrior', iniciative = 5, level = 3, icon = None, status = {})
    orc.initname('Orc', 'HasBro', sobral_45 = 0)

    monsters.append(orc)

    cpu1 = orc
    who_move = player1

# Определение рож персонажей игры.
define e = Character('Эйлин', color="#c8ffc8")


# Игра начинается здесь.
label start:
    python:
        curloc = 'loc_home' # Инициализация curloc
        field_generation()
        who_not_move = cpu1

    $ move('loc_create')
    return
