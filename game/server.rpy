init -10 python:
    import socket
    import thread
    import sys

    BUFF = 1024
    HOST = '127.0.0.1'# must be input parameter @TODO
    PORT = 8888 # must be input parameter @TODO
    socket_exit = 0
    socket_print = ''

    def response(key):
        return '123'

    def handler(clientsock,addr):
        global socket_print
        while 1:
            data = clientsock.recv(BUFF)
            socket_print += 'data: {}\n'.format(repr(data))
            if not data: break
            #my_responce = 'rune_x = {}, rune_y = {}'.format(str(rune_x), str(rune_y))
            my_responce = '1212121212121212'
            clientsock.send(response(my_responce))
            socket_print += 'sent: {}\n'.format(repr(response(my_responce)))
            # clientsock.close() # - reports [Errno 9] Bad file descriptor as it looks like that socket is trying to send data when it is already closed

    def create_cocket():
        global socket_print, socket_exit
        ADDR = (HOST, PORT)
        serversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        serversock.bind(ADDR)
        serversock.listen(5)
        socket_print += 'waiting for connection...\n'
        while socket_exit == 0:
            clientsock, addr = serversock.accept()
            socket_print += '...connected from: {}\n'.format(addr)
            thread.start_new_thread(handler, (clientsock, addr))
            socket_print += str(socket_exit) + '\n'
        else:
            socket_exit = 0
            return

    def client_connect():
        global socket_print
        s = None
        for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM):
            af, socktype, proto, canonname, sa = res
            try:
                s = socket.socket(af, socktype, proto)
            except socket.error as msg:
                s = None
                continue
            try:
                s.connect(sa)
            except socket.error as msg:
                s.close()
                s = None
                continue
            break
        if s is None:
            socket_print = 'could not open socket'
            return
        s.sendall('123')
        data = s.recv(BUFF)
        s.close()
        socket_print += 'Received: 123 {}\n'.format(data)


