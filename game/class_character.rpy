init -2 python:
    class Game_Character:
        global mage_skillbook, archer_skillbook, warrior_skillbook, cleric_skillbook, fairy_magicbook

        ## Class skills
        warrior_skillbook = []
        cleric_skillbook = []
        archer_skillbook = []
        mage_skillbook = []

        ## fairie skills
        fayrie_fire_skill_list = []
        
        character_magicbook = {'warrior':warrior_skillbook, 'cleric':cleric_skillbook, 'archer':archer_skillbook, 'mage':mage_skillbook}

        def __init__(self, mana, money, exp, maxlives, lives, char_class, iniciative, level, icon, status={}):
            self.mana = mana
            self.money = money
            self.maxlives = maxlives
            self.exp = exp
            self.lives = lives ## продумать моментЮ у феи должны быть свои жизни
            self.char_class = char_class
            self.iniciative = iniciative
            self.level = level
            self.icon = icon
            self.status = status

        def __repr__(self):
            return '<{} name: "{}">'.format(self.__class__.__name__,
                                            self.__name__('utf-8'))

        def return_mana(self, stihiya):
            stihii = {'wind':0, 'flame':1, 'earth': 2, 'water':3}
            return self.mana[stihii[stihiya]]

        def return_my_mana(self,number):
            return self.mana[number]

        def return_status(self):
            return self.status

        def add_status(self,statusname,moves):
            self.status[statusname] = moves

        def add_mana(self,stihiya,points):
            stihii = {'wind':0, 'flame':1, 'earth': 2, 'water':3}
            self.mana[stihii[stihiya]] += points

        def return_money(self):
            return self.money

        def return_icon(self):
            return self.icon

        def set_icon(self,new_icon_path):
            self.icon = new_icon_path

        def return_maxlives(self):
            return self.maxlives

        def add_maxlives(self,how_mach):
            self.maxlives += how_mach

        def add_money(self,points):
            self.money += points

        def return_lives(self):
            return self.lives

        def return_level(self):
            return self.level

        def add_lives(self,points):
            self.lives += points

        def return_exp(self):
            return self.exp

        def add_exp(self,points):
            self.exp += points

        def return_skillbook(self):
            return Game_Character.character_magicbook[self.char_class]

        def return_fairybook(self):
            return fairy_magicbook

        def return_char_class(self):
            return self.char_class

    class Feya(Game_Character):
        def initname(self, name, fname, sobral_45, feyaexp, feyalives):
            self.fname = fname
            self.name = name
            self.sobral_45 = sobral_45
            self.feyaexp = feyaexp
            self.feyalives = feyalives

        def get_sobral_45(self):
            return self.sobral_45



    class Monster(Game_Character):
        def initname(self, name, fname, sobral_45):
            self.fname = fname
            self.name = name
            self.sobral_45 = sobral_45

        def get_sobral_45(self):
            return self.sobral_45